FROM python:3.12
RUN pip install poetry

WORKDIR /app

COPY pyproject.toml /app/

RUN poetry config virtualenvs.create false \
  && poetry install --no-interaction --no-ansi

COPY . /app/

EXPOSE 8080

# Launch FastAPI app using Uvicorn
CMD ["uvicorn", "state_server:app", "--host", "0.0.0.0", "--port", "8080"]
