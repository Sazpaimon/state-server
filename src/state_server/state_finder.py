"""
State finder endpoint logic
"""

from typing import List, Tuple, Dict

from fastapi import APIRouter, Form
from starlette.responses import PlainTextResponse


class StateFinder:
    """
    Endpoint which will return a state (if any) given lat/long
    """
    states_data: Dict[str, Dict[str, List[Tuple[float, float]] |
                                Tuple[float, float, float, float]]]

    def __init__(
        self,
        states: List[Dict[str, str | List[Tuple[float, float]]]]
    ) -> None:
        """
        Parses the states location DB and generate bounding boxes

        :param states: State location data
        """
        self.states_data = {}
        for state in states:
            border = state['border']
            if isinstance(border, list):
                bounding_box = self._calculate_bounding_box(border)
                self.states_data[str(state['state'])] = {
                    'border': border,
                    'bounding_box': bounding_box
                }
        self.router = APIRouter()
        self.setup_routes()

    def setup_routes(self):
        """
        Sets up the GET/POST routes for the state finder
        """
        @self.router.get("/")
        async def get_state(longitude: float, latitude: float):
            return await self.get_state(longitude, latitude)

        @self.router.post("/")
        async def post_state(longitude: float = Form(...), latitude: float = Form(...)):
            return await self.get_state(longitude, latitude)

    @staticmethod
    def _calculate_bounding_box(
        border: List[Tuple[float, float]]
    ) -> Tuple[float, float, float, float]:
        """
        Calculate the bounding box for a given state's borders.
        """
        long_coords, lat_coords = zip(*border)
        return min(long_coords), min(lat_coords), max(long_coords), max(lat_coords)

    @staticmethod
    async def _point_in_bounding_box(
        lon: float,
        lat: float,
        bounding_box: Tuple[float, float, float, float]
    ) -> bool:
        """
        Check if a point is inside a bounding box.
        """
        min_lon, min_lat, max_lon, max_lat = bounding_box
        return min_lon <= lon <= max_lon and min_lat <= lat <= max_lat

    @staticmethod
    async def _point_in_state(
        longitude: float, latitude: float, boundary: List[Tuple[float, float]]
    ) -> bool:
        """
        Check if a location is within or on the boundary of a region.
        """
        n = len(boundary)
        is_inside = False

        for i in range(n):
            lon1, lat1 = boundary[i]
            lon2, lat2 = boundary[(i + 1) % n]

            # Check if the location is on a boundary point
            if (longitude, latitude) in [(lon1, lat1), (lon2, lat2)]:
                return True  # Location is on a boundary point

            # Check if the location is on a boundary line
            if (lat1 <= latitude <= lat2 or lat2 <= latitude <= lat1) and (
                lon1 <= longitude <= lon2 or lon2 <= longitude <= lon1
            ):
                if (lat2 - lat1) * (longitude - lon1) == (latitude - lat1) * (lon2 - lon1):
                    return True  # Location is on a boundary line

            # Standard point-in-polygon check
            if ((lat1 <= latitude < lat2) or (lat2 <= latitude < lat1)) and (
                longitude < (lon2 - lon1) * (latitude - lat1) / (lat2 - lat1) + lon1
            ):
                is_inside = not is_inside

        return is_inside

    async def find_matching_state(
            self,
            longitude: float,
            latitude: float
    ) -> List[str]:
        """
        Finds a matching state given the longitude and latitude

        :return: 0 or more states that the latitude
                 and longitude intersect with
        """
        matching_states: List[str] = []
        for state, data in self.states_data.items():
            if await self._point_in_bounding_box(
                    longitude,
                    latitude,
                    data['bounding_box']
            ):
                if await self._point_in_state(
                        longitude,
                        latitude,
                        data['border']
                ):
                    matching_states.append(state)
        return matching_states

    async def get_state(
            self,
            longitude: float,
            latitude: float
    ) -> PlainTextResponse | list[str]:
        """
        Main endpoint handler

        :return: List of matching states, or a 404 if no states match
        """
        matching_states = await self.find_matching_state(longitude, latitude)

        if not matching_states:
            return PlainTextResponse("State not found", status_code=404)
        return matching_states
