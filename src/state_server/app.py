"""
Main application module for the State Server.
"""
import json
import os

from fastapi import FastAPI

from state_server.state_finder import StateFinder

app = FastAPI()

script_dir = os.path.dirname(os.path.abspath(__file__))
json_file = os.path.join(script_dir, 'states.json')

with open(json_file, 'r', encoding="utf-8") as f:
    states_data = json.load(f)
state_finder = StateFinder(states_data)

app.include_router(state_finder.router)
