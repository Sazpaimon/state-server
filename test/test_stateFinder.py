from unittest.mock import patch

import pytest
from httpx import AsyncClient

from state_server import app, StateFinder

mock_data = [
    {
        "state": "foo",
        "border": [
            [0, 0],
            [0, 1],
            [2, 1],
            [2, 0]
        ],
    },
    {
        "state": "bar",
        "border": [
            [0, 1],
            [0, 2],
            [1, 2],
            [1, 1]
        ],
    },
    {
        "state": "baz",
        "border": [
            [1, 1],
            [1, 2],
            [2, 0],
            [2, 1],
            [3, 0],
            [3, 2]
        ],
    }
]

test_data = [
    (0.5, 0.5, ["foo"]),
    (0.5, 1.5, ["bar"]),
    (2.5, 1, ["baz"]),
    (0.5, 1, ["foo", "bar"]),
    (1, 1, ["foo", "bar", "baz"]),
    (3, 3, [])
]


@pytest.fixture
def anyio_backend():
    return 'asyncio'


@pytest.mark.anyio
@pytest.mark.parametrize("longitude, latitude, expected", test_data)
async def test_find_matching_state(longitude, latitude, expected):
    state_finder = StateFinder(mock_data)
    assert expected == await state_finder.find_matching_state(longitude, latitude)


@pytest.mark.anyio
@patch('state_server.state_finder.StateFinder.find_matching_state')
async def test_get_state(mock_find_state):
    mock_find_state.return_value = ['foo']
    async with AsyncClient(app=app, base_url="http://test") as client:
        response = await client.get('/', params={'longitude': 0, 'latitude': 0})
    assert response.status_code == 200
    assert response.json() == ['foo']

    async with AsyncClient(app=app, base_url="http://test") as client:
        response = await client.post('/', data={'longitude': 0, 'latitude': 0})
    assert response.json() == ['foo']

    assert response.status_code == 200


@pytest.mark.anyio
@patch('state_server.state_finder.StateFinder.find_matching_state')
async def test_get_state_404(mock_find_state):
    mock_find_state.return_value = []
    async with AsyncClient(app=app, base_url="http://test") as client:
        response = await client.get('/', params={'longitude': 0, 'latitude': 0})
    assert response.status_code == 404
    assert response.text == 'State not found'

    async with AsyncClient(app=app, base_url="http://test") as client:
        response = await client.post('/', data={'longitude': 0, 'latitude': 0})
    assert response.status_code == 404
    assert response.text == 'State not found'
